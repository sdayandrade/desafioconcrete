﻿using WebApiAuthentication.API.Entities;
using WebApiAuthentication.API.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using WebApiAuthentication.API;

namespace WebApiAuthentication.API
{

    public class AuthRepository : IDisposable
    {
        private AuthContext authContext;
        private CustomizedUserManager userManager;

        public AuthRepository()
        {
            authContext = new AuthContext();
            userManager = new CustomizedUserManager(new UserStore<CustomizedUser>(authContext));
        }

        public async Task<IdentityResult> SingUp(SingUpModel userModel)
        {
            CustomizedUser user = BindUserModelToCustomizedUser(userModel);
            return await userManager.CreateAsync(user, userModel.Senha);
        }

        private static CustomizedUser BindUserModelToCustomizedUser(SingUpModel userModel)
        {
            return new CustomizedUser()
            {
                UserName = userModel.Nome,
                PhoneNumber = ConvertListPhonesToString(userModel.Telefones),
                Email = userModel.Email,
                CreationDate = DateTimeOffset.Now
            };
        }

        private static string ConvertListPhonesToString(List<Phone> phones)
        {
            var phonesString = "";
            foreach (Phone phone in phones)
            {
                phonesString = String.Concat(phone.ToString() + ";", phonesString);
            }
            return phonesString;
        }

        public async Task<CustomizedUser> FindUser(string userName, string password)
        {
            return await userManager.FindAsync(userName, password);
        }

        public async Task<CustomizedUser> FindByEmailAsync(string email)
        {
            return await userManager.FindByEmailAsync(email);
        }

        public PasswordVerificationResult VerifyPassword(string hashedPassword, string providedPassword)
        {
          return userManager.PasswordHasher.VerifyHashedPassword(hashedPassword, providedPassword);
        }

        public async Task<CustomizedUser> FindAsync(UserLoginInfo loginInfo)
        {
            CustomizedUser user = await userManager.FindAsync(loginInfo);

            return user;
        }

        public async Task<IdentityResult> CreateAsync(CustomizedUser user)
        {
            var result = await userManager.CreateAsync(user);

            return result;
        }

        public async Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            var result = await userManager.AddLoginAsync(userId, login);

            return result;
        }

        public void Dispose()
        {
            authContext.Dispose();
            userManager.Dispose();

        }
    }
}