// <auto-generated />
namespace WebApiAuthentication.API.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class GenerateInitialTables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(GenerateInitialTables));
        
        string IMigrationMetadata.Id
        {
            get { return "201601231205456_GenerateInitialTables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
