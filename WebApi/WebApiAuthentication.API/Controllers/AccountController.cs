﻿using WebApiAuthentication.API;
using WebApiAuthentication.API.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using WebApiAuthentication.API.Entities;
using WebApiAuthentication.API.Models;
using WebApiAuthentication.API.Results;
using System.Net;
using System.Web.Http.Results;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace WebApiAuthentication.API.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private AuthRepository authRepository = null;

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        public AccountController()
        {
            authRepository = new AuthRepository();
        }

        [AllowAnonymous]
        [Route("SingUp")]
        public async Task<IHttpActionResult> SingUp(SingUpModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

             IdentityResult result = await authRepository.SingUp(userModel);

             if (!result.Succeeded)
             {
                 return GetErrorResult(result);
             }

             CustomizedUser userCreated = await authRepository.FindUser(userModel.Nome, userModel.Senha);

             return Json(CreateSingUpResultByUserCreated(userCreated));
        }

        [AllowAnonymous]
        [Route("SignIn")]
        public async Task<IHttpActionResult> SignIn(SignInModel userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CustomizedUser userCreated = await authRepository.FindByEmailAsync(userModel.Email);
            if (userCreated == null)
            {
                return Content(HttpStatusCode.Unauthorized, Resources.UsuarioEOuSenhaInvalidos);
            }
            else 
            {
                var verifiedPassword = authRepository.VerifyPassword(userCreated.PasswordHash, userModel.Senha);

                if (!verifiedPassword.Equals(PasswordVerificationResult.Success))
                    return Content(HttpStatusCode.Unauthorized, Resources.UsuarioEOuSenhaInvalidos);
                else
                    return Json(CreateSingUpResultByUserCreated(userCreated));
            } 
        }

        [HttpGet]
        [Authorize]
        [Route("FindUser")]
        public IHttpActionResult FindUser()
        {
           // TO DO

            return Ok();
        }

        private SingUpResult CreateSingUpResultByUserCreated(CustomizedUser userCreated) 
        {
            return new SingUpResult
            {
                Nome = userCreated.UserName,
                Email = userCreated.Email,
                Senha = userCreated.PasswordHash,
                Telefones = userCreated.PhoneNumber,
                DataCriacao = userCreated.CreationDate,
                DataAtualizacao = userCreated.UpdateDate,
                UltimoLogin = userCreated.LastLogin,
                Token = userCreated.Token
            };
        
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                authRepository.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private string GetQueryString(HttpRequestMessage request, string key)
        {
            var queryStrings = request.GetQueryNameValuePairs();

            if (queryStrings == null) return null;

            var match = queryStrings.FirstOrDefault(keyValue => string.Compare(keyValue.Key, key, true) == 0);

            if (string.IsNullOrEmpty(match.Value)) return null;

            return match.Value;
        }

        private JObject GenerateLocalAccessTokenResponse(string userName)
        {

            var tokenExpiration = TimeSpan.FromDays(1);

            ClaimsIdentity identity = new ClaimsIdentity(OAuthDefaults.AuthenticationType);

            identity.AddClaim(new Claim(ClaimTypes.Name, userName));
            identity.AddClaim(new Claim("role", "user"));

            var props = new AuthenticationProperties()
            {
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.Add(tokenExpiration),
            };

            var ticket = new AuthenticationTicket(identity, props);

            var accessToken = Startup.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);

            JObject tokenResponse = new JObject(
                                        new JProperty("userName", userName),
                                        new JProperty("access_token", accessToken),
                                        new JProperty("token_type", "bearer"),
                                        new JProperty("expires_in", tokenExpiration.TotalSeconds.ToString()),
                                        new JProperty(".issued", ticket.Properties.IssuedUtc.ToString()),
                                        new JProperty(".expires", ticket.Properties.ExpiresUtc.ToString())
        );

            return tokenResponse;
        }

        #endregion
    }
}
