﻿using WebApiAuthentication.API.Entities;
using WebApiAuthentication.API.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApiAuthentication.API
{
    public class AuthContext : IdentityDbContext<CustomizedUser>
    {
        static AuthContext()
        {
            Database.SetInitializer<AuthContext>(null);
        }
        
        public AuthContext()
            : base("AuthContext")
        {
            
        }

    }

}