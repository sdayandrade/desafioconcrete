﻿
namespace WebApiAuthentication.API.Entities
{
    public class Phone
    {
        public string Numero { get; set; }
        public string DDD { get; set; }

        public override string ToString()
        {
            return Numero + "," + DDD;
        }
    }
}