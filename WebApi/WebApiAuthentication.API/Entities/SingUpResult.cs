﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiAuthentication.API.Entities
{
    public class SingUpResult
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Telefones { get; set; }
        public DateTimeOffset DataCriacao { get; set; }
        public DateTimeOffset DataAtualizacao { get; set; }
        public DateTimeOffset UltimoLogin { get; set; }
        public Guid Token { get; set; }
    }
}