﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiAuthentication.API;
using WebApiAuthentication.API.Models;

namespace WebApiAuthentication.API
{
    public class CustomizedUserManager : UserManager<CustomizedUser>
    {
        public const string PURPOSE = "ASP.NET Identity";

        public CustomizedUserManager(IUserStore<CustomizedUser> store)
            : base(store)
        {
        }

        public static CustomizedUserManager Create(IdentityFactoryOptions<CustomizedUserManager> options, IOwinContext context)
        {
            var manager = new CustomizedUserManager(new UserStore<CustomizedUser>(context.Get<AuthContext>()));

            manager.UserValidator = new UserValidator<CustomizedUser>(manager)
            {
                RequireUniqueEmail = true // Adicionar mensagem "E-mail já existente".
            };
            
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<CustomizedUser>(dataProtectionProvider.Create(PURPOSE));
            }
            return manager;
        }
    }
}