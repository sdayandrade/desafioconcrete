﻿using WebApiAuthentication.API.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApiAuthentication.API.Models
{
    public class SingUpModel
    {
        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Senha")]
        public string Senha { get; set; }

        [Display(Name = "Data de Criacao")]
        public string DataCriacao { get; set; }

        [Required]
        [Display(Name = "Telefones")]
        public List<Phone> Telefones { get; set; }
    }

}